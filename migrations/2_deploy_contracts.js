const PointcoinToken = artifacts.require('PointcoinToken');
const SafeMath = artifacts.require('SafeMath.sol');
const tokenConfig = require('../contracts-params').token;


module.exports = function(deployer, network, accounts) {

    //web3.personal.unlockAccount(accounts[0],"testtest","0x15000");
    let tokenContractInstance;

    deployer.deploy(SafeMath, {overwrite: false})
        .then(function() {
            return deployer.link(SafeMath, [PointcoinToken]);
        })
        .then(function() {
            return deployer.deploy(PointcoinToken, tokenConfig.name, tokenConfig.symbol, tokenConfig.decimals, tokenConfig.totalSupply, tokenConfig.wallet)
                .then(function() {
                    return PointcoinToken.deployed();
                });
        });
};
