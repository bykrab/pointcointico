pragma solidity ^0.4.17;

import "./token/DetailedERC20.sol";
import "./token/StandardToken.sol";
import "./ownership/Ownable.sol";

contract PointcoinToken is DetailedERC20, StandardToken, Ownable {

    function PointcoinToken(string _name, string _symbol, uint8 _decimals, uint _totalSupply, address wallet)
    DetailedERC20(_name, _symbol, _decimals)
    public {
        owner = msg.sender;
        totalSupply = _totalSupply;

        balances[wallet] = _totalSupply;
        Transfer(0x0, wallet, _totalSupply);
    }


}
