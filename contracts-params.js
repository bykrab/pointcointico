

module.exports = {
    token: {
        name: "Point Coin Token",
        symbol: "PTN",
        decimals: 18,
        totalSupply: (1000000000 * Math.pow(10, 18)).toString(),
        wallet: "0xD25299F777663481Bf3F5f1Eb21b6b2dd861f0db"
    }
};
